(function(){
	var doc = $(document),
		win = $(window);
		
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();
	var counter = function(operator, elem) {
		var i = elem.val();
		if ( operator == '-' ) {
			if ( elem.val() <= 1 ) {
				return;
			} else {
				i--;
				elem.val(i)
			}
		}
		if ( operator == '+' ) {
			i++;
			elem.val(i);
		}
		elem.trigger('change');
	};
	var toggleClass = function() {
		var elem = $('[data-toggle]');
		elem.click(function() {
			var _this = $(this);
			var id = _this.attr('data-target');
			var target = $('#'+id);
			if ( _this.attr('data-toggle') == 'quantity-minus' ) {
				counter('-', target)
				return;
			}
			if ( _this.attr('data-toggle') == 'quantity-plus' ) {
				counter('+', target)
				return;
			}
			target.toggleClass('visible');
		});
	};
	var contentIndents = function() {
		var elem = $('[data-fixed]');
		var id = elem.attr('data-target');
		var target = $('#'+id);
		var indents = elem.outerHeight();
		if ( elem.attr('data-fixed') == 'true' ) {
			target.css({'padding-top': indents});
		}
	};
	function setSelectList() {
		$('.single-select').select2({
		});

	}
	doc.ready(function() {
		svg4everybody({
			polyfill: true
		});
		toggleClass();
		contentIndents();
		setSelectList();
	});
	win.on('resize', function() {
		waitForFinalEvent(function() {
			contentIndents();
		}, 500, "resize")
	})
}())