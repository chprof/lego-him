module.exports = [
    "./gulp/tasks/html",
    "./gulp/tasks/styles",
    "./gulp/tasks/scripts",
    "./gulp/tasks/favicons",
    "./gulp/tasks/images",
    "./gulp/tasks/fonts",
    "./gulp/tasks/del",
    "./gulp/tasks/svgSprite.js",
    "./gulp/tasks/watch",
    "./gulp/tasks/serve",
    "./gulp/tasks/npmPlugins"
];