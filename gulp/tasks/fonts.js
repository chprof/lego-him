module.exports = function (done) {
	$.gulp.task("fonts", function () {
		return $.gulp.src("./app/fonts/*.{woff,woff2}")
			.pipe($.gp.newer('./build/fonts/'))
			.pipe($.gulp.dest("./build/fonts/"))
			// .on("end", done);
	});
};