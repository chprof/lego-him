module.exports = function () {
	$.gulp.task("cssVendor", function (done) {
		return $.gulp.src("./app/plugins/*.css")
			.pipe($.gp.concat('vendor.css'))
			.pipe($.gp.cleanCss({
				rebase: false
			}))
			.pipe($.gp.rename({ suffix: ".min" }))
			.pipe($.gulp.dest("./build/css/"))
			.on('end', done);
	});
	$.gulp.task("jsVendor", function (done) {
		return $.gulp.src("./app/plugins/*.js")
			.pipe($.gulp.dest("./build/js/libs/"))
			.on('end', done);
	});
	$.gulp.task("fontVendor", function (done) {
		return $.gulp.src("./app/plugins/*.{woff,woff2}")
			.pipe($.gulp.dest("./build/fonts/libs/"))
			.on('end', done);
	});
	$.gulp.task('vendors', $.gulp.series('cssVendor', 'jsVendor', 'fontVendor'));
};