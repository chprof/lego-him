module.exports = function () {
	$.gulp.task('npmPlugins', function(done) {
		// return console.log($.npmfiles({
		// 	nodeModulesPath: '../../node_modules/'
		// }))
		return $.gulp.src($.npmfiles({
			nodeModulesPath: '../../node_modules/'
		})).pipe($.gulp.dest('./app/js/libs')).on('end', done);
	})
};